# CI/CD test/learning pipeline

[![Build Status](https://gitlab.com/frizner/cicdtest/badges/master/build.svg)](https://gitlab.com/frizner/cicdtest/commits/master)
[![Coverage Report](https://gitlab.com/frizner/cicdtest/badges/master/coverage.svg)](https://gitlab.com/frizner/cicdtest/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/frizner/cicdtest)](https://goreportcard.com/report/gitlab.com/frizner/cicdtest)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

## Creating the container

```sh
docker build -t registry.gitlab.com/frizner/cicdtest:latest -t registry.gitlab.com/frizner/cicdtest:clang-6.0 tools/.
docker push registry.gitlab.com/frizner/cicdtest
```

## Create helm deployment

```sh
helm install --name infoweb tools/infoweb-helm --set ENVIRONMENT=stage,VERSION=v0.0.1,COMMIT="16db162d2de7a716a4cc4dd8bd6d8f1489cdc049"
```

## To upgrade

```sh
helm upgrade infoweb tools/infoweb-helm --set ENVIRONMENT=stage,VERSION=v0.0.1,COMMIT="16db162d2de7a716a4cc4dd8bd6d8f1489cdc049"
```

## To remove

```sh
helm del --purge infoweb
```
