package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestSum(t *testing.T) {
	// Correct operands
	a := 3
	b := 2
	c := 100

	// Test request
	path := fmt.Sprintf("/sum?a=%d&b=%d&c=%d", a, b, c)
	req, err := http.NewRequest(http.MethodGet, path, nil)
	if err != nil {
		t.Fatal(err)
	}

	// Handle request
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(sum)
	handler.ServeHTTP(rr, req)

	// Check status code
	if rr.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", rr.Code, http.StatusOK)
	}

	// Check header
	if ctype := rr.Header().Get("Content-Type"); ctype != "application/json" {
		t.Errorf("content type header does not match: got %v want %v", ctype, "application/json")
	}

	// Check answer
	expected := fmt.Sprintf("{\"sum\":%d}", a+b+c)
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}

	// Incorrect amount operands
	// Test request
	path = fmt.Sprintf("/sum?a=%d", a)
	req, err = http.NewRequest(http.MethodGet, path, nil)
	if err != nil {
		t.Fatal(err)
	}

	// Handle request
	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(sum)
	handler.ServeHTTP(rr, req)

	// Check status code
	if rr.Code != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v", rr.Code, http.StatusBadRequest)
	}

	// Check header
	if ctype := rr.Header().Get("Content-Type"); ctype != "application/json" {
		t.Errorf("content type header does not match: got %v want %v", ctype, "application/json")
	}

	// Check answer
	expected = fmt.Sprintf("{\"error\": \"Amount of parameters must be greater than 1\"}")
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}

	// Incorrect operand
	// Test request
	d := "3.1"
	path = fmt.Sprintf("/sum?a=%d&d=%s", a, d)
	req, err = http.NewRequest(http.MethodGet, path, nil)
	if err != nil {
		t.Fatal(err)
	}

	// Handle request
	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(sum)
	handler.ServeHTTP(rr, req)

	// Check header
	if ctype := rr.Header().Get("Content-Type"); ctype != "application/json" {
		t.Errorf("content type header does not match: got %v want %v", ctype, "application/json")
	}

	// Check status code
	if rr.Code != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v", rr.Code, http.StatusBadRequest)
	}

	// Check answer
	expected = fmt.Sprintf("{\"error\": \"'d' must have integer numeric value\"}")
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}
}
