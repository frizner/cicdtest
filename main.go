package main

import (
	"fmt"
	"io"
	"log"
	"math/rand"
	"net"
	"net/http"
	"os"
	"strconv"
	"time"
)

const (
	listen              = ":8080"
	envVersion          = "VERSION"
	dftReadinessTimeOut = 10 * time.Second
	envOKProba          = "OKPROBE"     // Name of env. to set a likelihood of not OK
	dftOKProba          = 0.2           // Default likelihood of not OK for a health check
	envEnv              = "ENVIRONMENT" // Name of env. for the current environment
	envCommit           = "COMMIT"      // Name of env. var. for the current commit
)

var (
	readinessCh chan bool
)

func infoPage(w http.ResponseWriter, r *http.Request) {
	version := os.Getenv(envVersion)

	hostName, err1 := os.Hostname()
	if err1 != nil {
		fmt.Fprintf(os.Stderr, "Version: %s\n%s\n", version, err1)
		http.Error(w, err1.Error(), http.StatusInternalServerError)
		return
	}
	addrs, err2 := net.InterfaceAddrs()
	if err2 != nil {
		fmt.Fprintf(os.Stderr, "Version: %s\n%s\n", version, err2)
		http.Error(w, err2.Error(), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(w, "Hostname: %s\nIP adressess: %s\nVersion: %s\n", hostName, addrs, version)

	if env := os.Getenv(envEnv); env != "" {
		fmt.Fprintf(w, "Environment: %s\n", env)
	}

	if env := os.Getenv(envCommit); envCommit != "" {
		fmt.Fprintf(w, "Commit: %s\n", env)
	}
}

func healthChk(w http.ResponseWriter, r *http.Request) {
	strOkProbe := os.Getenv(envOKProba)
	OKproba, err := strconv.ParseFloat(strOkProbe, 32)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err.Error())
		OKproba = dftOKProba
	}

	flip := rand.Float64()
	if flip < OKproba {
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		io.WriteString(w, `{"status": "ok"}`)
	} else {
		http.Error(w, "Service inaccessible", http.StatusInternalServerError)
	}
}

func readinessChk(w http.ResponseWriter, r *http.Request) {
	select {
	case <-readinessCh:
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		io.WriteString(w, `{"readiness": "ready"}`)
		return
	default:
		http.Error(w, "Not ready", http.StatusServiceUnavailable)
		return
	}
}

func sum(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := r.URL.Query()
	fmt.Println(params)

	if len(params) < 2 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "{\"error\": \"Amount of parameters must be greater than 1\"}")
		return
	}

	sum := 0
	for k, v := range params {
		i, err := strconv.Atoi(v[0])
		if err != nil {
			errMsg := fmt.Sprintf("{\"error\": \"'%s' must have integer numeric value\"}", k)
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(w, errMsg)
			return
		}
		sum += i
	}
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "{\"sum\":%d}", sum)
}

func main() {
	readinessCh = make(chan bool)
	go func() {
		time.Sleep(dftReadinessTimeOut)
		close(readinessCh)
	}()

	http.HandleFunc("/", infoPage)
	http.HandleFunc("/health", healthChk)
	http.HandleFunc("/readiness", readinessChk)
	http.HandleFunc("/sum", sum)
	if err := http.ListenAndServe(listen, nil); err != nil {
		log.Fatalf("%s", err)
	}
}
