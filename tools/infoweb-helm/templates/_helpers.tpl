{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "infoweb.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "infoweb.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "infoweb.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Define the name of the LoadBalancer service
*/}}
{{- define "infoweb.loadbalancer-service-name" -}}
{{- printf "%s-%s" .Release.Name "lb" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Define the name of the horizonral pod autoscaler
*/}}
{{- define "infoweb.hpd" -}}
{{- printf "%s-%s" .Release.Name "hpd" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
  Define the labels that should be used by selectors
*/}}
{{- define "infoweb.selector" -}}
app: {{ template "infoweb.name" . }}
env: {{ .Values.ENVIRONMENT }}
{{- end -}}

{{- define "infoweb.labels" -}}
app: {{ template "infoweb.name" . }}
env: {{ .Values.ENVIRONMENT }}
helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}